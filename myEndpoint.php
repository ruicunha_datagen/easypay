<?php

/*
If you are using composer, include the autoload file. Otherwise, just include the Easypay class file. */
include_once "../vendor/autoload.php";
// include_once "easypay.php";



$incoming = new Easypay();
$process = $incoming -> processPaymentNotification();
if( $process['status']===true ){
	$response=array(
		"status"=>"ok",
		"message"=>array("The payment was successfully processed")
	);
}else{
	$response=array(
		"status"=>"error",
		"message"=>array("There was an error while processing this notification.")
	);
}

header("Content-type:application/json");
echo json_encode($response);
die();
?>