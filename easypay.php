<?php

/*
For this class purposes:
CLIENT -> is the person from which you want to receive money. Tipically you have made a commercial transaction with him and expect to be paid.
REQUEST -> is your call to the Easypay endpoint with specific details about your transaction, so that they know what to process and how to notify you back once your client pays. Their response to you will contain a set of payment references. You need to let your client know about those, so that he can use to pay.
NOTIFICATION -> is a call from Easypay to an endpoint provided by you, letting you know that a client has paid, with information about that specific payment. You should handle this as an event, within a microservice logic.
*/

class Easypay
{

	/*
	When Easypay notifies your endpoint about a payment this event is first registred in a text file on your filesystem, before processing. If you process this payment correctly, the file is deleted at the end. Otherwise, another file with the same timestamp is written, containing information about the error. This is the directory where such files are placed. */
	public $notificationsDirectory = __DIR__ . "/notifications/";
	
	/*
	You can find these on Easypay's backoffice, once you have an account.
	There will be different ones for development and production.*/
	public $easypayAccountId;
	public $easypayApiKey;
	public $easypayEndpointUrl;
	
	/*
	Specific info about each request. */
	public $requestId;	// your own ID for this request
	public $paymentAmount;	// the transaction amount
	public $documentId;	// your internal ID of the document concerning this transaction
	public $documentNumber; // your document number 
	public $documentDate;	// your document date, on the format 'YYYY-MM-DD'
	public $clientName;
	public $clientEmail;
	public $clientVatNumber;
	

	public function requestReference(){
		
		// all properties on the request need to be utf8 encoded
		$vars= array( 'requestId', 'paymentAmount', 'documentId', 'documentNumber', 'documentDate', 'clientName', 'clientEmail', 'clientVatNumber');
		foreach ( $vars as $varName ){
			if ( !empty($$varName) && mb_detect_encoding($$varName, 'UTF-8', false) ){
				return array(
					"status"=>false,
					"message"=>"The property $varName is not UTF-8 Encoded"
				);
			}
		}
		
		if ( ! $this->validateDate( $this->documentDate, $format = 'Y-m-d' ) ){
			return array(
				"status"=>false,
				"message"=> "The document date does not have a valid format" 
			);
		}
		
		$body = array(
			"key" => $this->requestId,
			"method" => "mb",
			"type"	=> "sale",
			"value"	=> floatval( $this->paymentAmount ),
			"currency"	=> "EUR",
			"capture" => array(
				"transaction_key" => (string) $this->documentId,
				"descriptive" => "Proforma n." . $this->documentNumber,
				"capture_date" => (string) $this->documentDate
			),
			"customer" => array(
				"name" => (string) $this->clientName,
				"email" => (string) $this->clientEmail,
				"key" => "------------",
				"phone_indicative" => "+351",
				"phone" => "999999999",
				"fiscal_number" => (string) $this->clientVatNumber,
			)
		);

		$headers = array(
			"AccountId: " . $this->easypayAccountId,
			"ApiKey: " . $this->easypayApiKey,
			"Content-Type: application/json"
		);
		$curlOpts = [
			CURLOPT_URL => $this->easypayEndpointUrl,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => 1,
			CURLOPT_TIMEOUT => 60,
			CURLOPT_POSTFIELDS => json_encode($body),
			CURLOPT_HTTPHEADER => $headers,
		];

		$curl = curl_init();
		curl_setopt_array($curl, $curlOpts);
		$responseBody = curl_exec($curl);
		curl_close($curl);
		
		$response = json_decode($responseBody, true);
		if ($response["status"]==="ok"){
			$result=array(
				"status"=>true,
				"message"=>"The payment reference was generated successfully.",
				"entity"=>$response["method"]["entity"],
				"reference"=>$response["method"]["reference"],
				"amount"=>number_format($body["value"],2),
				"easypayPaymentId"=>$response['id'],
				"easypayClientId"=>$response['customer']['id'],
				"easyPayFullResponse"=>$responseBody
				);
		}else{
			$result=array(
				"status"=>false,
				"message"=>"There was an error. Check the remote response for details.",
				"easyPayFullResponse"=>$response
				);
		}
		return $result;
	}

	public function processPaymentNotification(){
		
		$notification=json_decode(file_get_contents('php://input'), true);
		
		// writes the notification to disk
		if( !is_dir( $this->notificationsDirectory )) {
			mkdir( $this->notificationsDirectory );
		}
		$file=$this->notificationsDirectory . microtime(true);
		file_put_contents( $file . ".notification.txt" , $notification );
		
		try{



			/*
			Here you need to process the payment in your system, with your own code.
			Handle errors and raise Exceptions for them, containing detailed information about the error. Remember you will need that detail, if something goes wrong.
			*/
			
			
			
			
			//Payment process was sucessfull, we can delete the notification from the disk
			unlink($file . ".notification.txt");
			return array(
				"status"=>true,
				"message"=>'Payment was processed with success.'
			);
			
		}catch(exception $e){
			
			$errorLog= $notification . chr(10) . chr(10) . "There was an error while processing this notification from Easypay." . chr(10) . $e->getMessage();
			
			file_put_contents( $file . ".errorLog.txt" , $errorLog );
			
			return array(
				"status"=>false,
				"message"=>'A payment notification was received, but there was an error while processing it.' . chr(10) . $e->getMessage()
			);
			
		}
		
	}

	private function validateDate($date, $format = 'Y-m-d')	{
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}
}