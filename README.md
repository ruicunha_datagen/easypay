###### Credits
A lot of this code was actually taken from Easypay's documentation. I just turned it into a class, providing an abstraction layer that moves its logic out of the application code. 

###### Author: Rui Cunha
###### Email: r.cunha@datagen.eu

### About this class
Easypay is a portuguese company that provides multiple payment services: atm, credit cards, transfers and more. They provide full documentation about their webservices, and if you are going to use their services that is really your place to start. This class is just a pratical implementation of their code, allowing the developer to abstract from it's logic and write cleaner, readable code on the application itself.

###### How it works
Just instantiate the class, and provide the data.
You will find your Account Id and API Key in their backoffice, once you have been provided access to it. The endpoint URL used bellow is the one used for testing, you will need to change it on production.

```php
$call=new Easypay();
$call->easypayAccountId='XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX' ;
$call->easypayApiKey= 	'YYYYYYYY-YYYY-YYYY-YYYY-YYYYYYYYYYYY';
$call->easypayEndpointUrl= 'https://api.test.easypay.pt/2.0/single';

$call->requestId= '9999999999';	/*your identifier for this request*/
$call->paymentAmount= 17.57;
$call->documentId= '34534534';	/*your identifier for the document generating this transaction*/
$call->documentNumber= '234234';  /*your identifier document number*/
$call->documentDate='2015-06-28';	/* notice the format */
$call->clientName= 'Rui Manuel';	/* the person who is being requested to pay */
$call->clientEmail= 'someone@somewhere.com';
$call->clientVatNumber= '202333234';
$response = $call->requestReference();
```

###### How to set it up
Include the file easypay.php, if you are not using composer. If you are using composer, just type:
```shell
composer require hotworks/easypay
```
If all you need is to generate payment refences, you're good to go.

If want to be notified about payments and process such payment in your system, you will need to setup your own endpoint.
1. Copy the file myEndpoint.php to your public folder.
2. At the beggining of your new myEndpoint.php either include the composer autoload file or mannually include the Easypay class.
3. On the Easypay backoffice, provide them with the public URL of your myEndpoint.php file.
4. On the class file easypay.php, complete the method processPaymentNotification() with the code that will process your payment. Remember to handle errors and raise exceptions.

That's it.


###### To do
A lot more can be done using Easypay's webservices. 
Next: cancel existing references that you no longer want clients to pay.

